#include <stdio.h>
#include <math.h>

int kvadraticka(int a, int b, int c, double * x1, double * x2)
{
    int d = b * b - 4 * a * c;
    *x1 = (-b + sqrt(d)) / (2 * a);
    *x2 = (-b - sqrt(d)) / (2 * a);
    return d;
}

int main()
{
    double x1 = 0,
           x2 = 0;
    int a = 1,
        b = 1,
        c = -2;

    int d = kvadraticka(a, b, c, &x1, &x2);

    if (d < 0) {
        printf("Rovnice nema realne koreny\n");
    }
    else if (d == 0) {
        printf("Rovnice ma prave jeden koren %.2f\n", x1);
    } else {
        printf("Rovnice ma dva realne koreny %.2f a %.2f\n", x1, x2);
    }

    return 0;
}


