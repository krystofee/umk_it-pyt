#include <stdlib.h>
#include <stdio.h>

#define ALLOC_CONST (10)
#define NEWLINE     ('\n')
#define CHAR_ZERO   ('0')


int main()
{
    int i;
    int length = 0,
        size = ALLOC_CONST;
    int * pole = (int*) malloc(sizeof(int) * size);
    char c;
    int scanf_state;

    scanf_state = scanf("%c ", &c);
    while (scanf_state != EOF) {
        pole[length] = c - CHAR_ZERO;
        length ++;

        if (size % length == 0) {
            size += ALLOC_CONST;
            int * temp = (int*) realloc(pole, sizeof(int) * size);
            pole = temp;
        }

        scanf_state = scanf("%c ", &c);
    }

    printf("Nacetli jsme %d prvku\n", length);
    printf("Vypis pole: \n");
    for (i = 0; i < length; i++) {
        printf("pole[%d] == %d\n", i, pole[i]);
    }

    free(pole);
    return 0;
}


