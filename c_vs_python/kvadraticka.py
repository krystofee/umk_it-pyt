from math import sqrt


def kvadraticka(a, b, c):
    x1, x2 = (None, None)

    d = b * b - 4 * a * c

    if d < 0:
        pass
    elif d == 0:
        x1 = (-b + sqrt(d)) / (2 * a)
    else:
        x1 = (-b + sqrt(d)) / (2 * a)
        x2 = (-b - sqrt(d)) / (2 * a)

    return (x1, x2)

a, b, c = (1, 1, -2)
(x1, x2) = kvadraticka(a, b, c)

if x1 is None and x2 is None:
    print('Rovnice nema realne koreny')
elif x2 is None:
    print(f'Rovnice ma prave jeden koren {x1:.2f}')
else:
    print(f'Rovnice ma dva realne koreny {x1:.2f} a {x2:.2f}')


