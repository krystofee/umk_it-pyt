# Větvení
# porovnávání

4 < 5
4 > 4.9
7 == 7

# logické spojky
# not, and, or

4 == (4 or 7) > 2

4 and 7
1 or 7

(4 != 4) or (9 < 2) or (8 >= 8)
not 4 or False
not 0 and True

True < 9 #
False > -8 #

int(True)
int(False)
int(None) #
int()
str(True)
str(None)
float(True)

None is None
4 is 8
8 is not 4

a = 5
b = 0
if a > 2:
    b = 7
    print("7")
    print("78451")
else:
    b = b + 5

b = 7

b = b + 5 if a > 2 else 7

b # ?

cislo_vetsi_nez_9 = 9
if cislo_vetsi_nez_9 > 9:
    print('Ok')
elif cislo_vetsi_nez_9 == 9:
    print('Cislo je rovno 9 ...')
else:
    print('Neumis zadat cislo ...')
