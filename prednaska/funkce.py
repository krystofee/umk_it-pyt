from math import sqrt


def kvadraticka(a, b, c):
    x1, x2 = (None, None)

    determinant = b ** 2 - 4 * a * c
    if determinant < 0:
        pass
    elif determinant == 0:
        x1 = (-b + sqrt(determinant)) / 2 * a
    else:
        x1 = (-b + sqrt(determinant)) / 2 * a
        x2 = (-b - sqrt(determinant)) / 2 * a

    return (x1, x2)



if __name__ == '__main__':
    vstup = input("zadej koeficienty jako - A, B, C:")
    # vstup = "4, 8, 9"

    (a, b, c) = [int(x) for x in vstup.split(',')]

    (x1, x2) = kvadraticka(a, b, c)

    if x1 is None and x2 is None:
        print('nema reseni')
    elif x2 is None:
        print(f'ma jedno reseni ktere je {x1}')
    else:
        print(f'ma reseni {x1:.2f}, {x2}') # formatted strings
        print('ma reseni ' + str(x1) + ', ' + str(x2))
