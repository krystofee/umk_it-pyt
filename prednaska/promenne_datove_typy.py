# Python primitives:
# int

4

print(type(4))

n = 5
m = int(9.5)
p = m + n
m = m + n

print(p)
print(m)

# float

4.5

print(type(4.5))

q = 3.2
r = int(9.8)
print(q + r)

q = float(8.88)

# str

s = "Kryštof Řeháček"
print(type(s))
print(s)

print(s[0] + s[8])

# bool + None

True
False
None

bool(4)
bool(0)
bool(None)
bool(-8)

# Non-Primitives
# list

l = [1, 8, 9, 5, 6, 7, 58, 52]
print(type(l))
print(l)
print(l[0] + l[5] + l[-1])

# list slicing

print(l[:5]) # == l[0:5]
print(l[5:]) # == l[5:len(l) + 1]
print(l[5:len(l)]) # proč ne [5:len(l)] ?

print("Kryštof Řeháček"[:7])

l = [4, 8, 9, "Jákob", "Ráchel"]
l.append("#buďjakojákob")
l
l.pop()
l.remove(9)
l
m = [1, 2, 3]
l + m
l.append(l)
l.extend(m)
l[-1]

# tuple

t = (4, 8, 9, "Adam", "Eva")
t.append("další člen") # hmmm...

# dict

vyska_osoby = {
    'Kryštof': 48,
    'Adam': 15,
    'Eva': 88,
}

del vyska_osoby['Kryštof']
vyska_osoby['Adam']
vyska_osoby['Petr'] # ... hmm?

vyska_osoby.values()

vyska_osoby.items()

# set

vstup = [4, 8, 5, 6, 4, 3]
len(vstup)
vstup_set = set(vstup)
len(vstup_set)
vstup_set[2]

dalsi_set = set([4, 5, 6, 7, 8, 9])

dalsi_set.difference(vstup_set)
dalsi_set.union(vstup_set)
dalsi_set.intersection(vstup_set)

dalsi_set - vstup_set # difference
dalsi_set | vstup_set # union
vstup_set & dalsi_set # intersection