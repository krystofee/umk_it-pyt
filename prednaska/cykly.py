a = [1, 3, 7, 9, 10]

print(a[-4:2]) # len(a) - 4:2 == 5 - 4:2 == 1:2

4 * 4
4 ** 4

for cislo in a:
    print(cislo ** 2)

list(enumerate(a))

for index, cislo in enumerate(a):
    a[index] = cislo ** 2


a

a = [cislo ** 2 for cislo in a]


print(a)

bool([])
bool([1, 8])

i = 0
while i <= 10:
    i += 1
    print(a.pop())
    print(a)

print(list(range(0, 10, 3)))

for n in range(0, 10):
    print(n)
