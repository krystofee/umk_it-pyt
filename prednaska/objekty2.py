# přetěžování metod

class Logger:

    def log(self, message):
        print(message)


class CounterLogger(Logger):
    log_count = 0

    def log(self, message):
        super().log(message)
        self.log_count += 1


logger = Logger()
logger.log('Něco se hrozně pokazilo')

counter_logger = CounterLogger()
counter_logger.log('Zase se to pokazilo')
counter_logger.log('Už to budeee')
counter_logger.log('A zas to funguje')

print(counter_logger.log_count)


# přetěžování objektů

class BaseLogger(Logger):
    level = "BASE"

    def log(self, message):
        print(f'[{self.level}] {message}')


class InfoLogger(BaseLogger):
    level = "INFO"


base = BaseLogger()
base.log('BaseLogger test')

Logger = InfoLogger()
Logger.log('Test')


# vícenásobné dědění


class LogCounterMixin:
    log_count = 0

    def log(self, message):
        self.log_count += 1
        super().log(message)


class CounterInfoLogger(LogCounterMixin, InfoLogger):
#                       ^^^^^^^^^^^^^^^
#                       Mixin obohaující funkcionalitu
#                                        ^^^^^^^^^^
#                                        Základní class
    pass


counter_info_logger = CounterInfoLogger()
counter_info_logger.log('Informativní log')
print(counter_info_logger.log_count)


# diamantový problém

class A:
    a = 'A'


class B(A):
    a = 'B prepise'


class C(A):
    a = 'C prepise'


class D(C, B):
    b = 'D'


# D -> C, B
# C -> A
# B -> A
# D.a?


D.a


class D2(B, C):
    b = 'd'


D2.a

D2 = type('D2', (A, ), {})

D2.a

