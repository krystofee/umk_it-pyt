
class Zvire:
    jmeno = None
    vek = 0
    pocet_zivotu = 1

    def __init__(self, jmeno, vek, pocet_zivotu=1):
        self.jmeno = jmeno
        self.vek = vek
        self.pocet_zivotu = pocet_zivotu

    def vypis_se(self):
        if self.pocet_zivotu == 1:
            print(f'Jsem {self.jmeno} a mám {self.vek} roků a {self.pocet_zivotu} zivot')
        else:
            print(f'Jsem {self.jmeno} a mám {self.vek} roků a {self.pocet_zivotu} zivotu')

    def vydej_zvuk(self):
        print('!!řvu!!')

z = Zvire("generické zvíře", 5)
z.vypis_se()


class Pes(Zvire):

    def vydej_zvuk(self):
        print("haf")

p = Pes("alík", 12, 2)
p.vypis_se()
p.vydej_zvuk()


class Kocka(Zvire):

    def vydej_zvuk(self):
        print("Mňau")


class Medved(Zvire):

    def vydej_zvuk(self):
        print("Brum")


zoo = [
    Pes("Alík", 13),
    Kocka("Micka", 4, 7),
    Medved("Béďa", 12, 8),
]

for zvire in zoo:
    zvire.vypis_se()